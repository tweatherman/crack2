#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"
#include <unistd.h>

const int PASS_LEN = 20;        // Maximum any password will be
const int HASH_LEN = 33;        // Length of MD5 hash strings

// Given a target hash, crack it. Return the matching

char * crackHash(char * targetHash, char * dictionaryFilename)
{
    
    FILE *passwords = fopen(dictionaryFilename, "r");
        if(!passwords){
            fprintf(stderr, "Can't open file %s for reading", dictionaryFilename);
            perror(NULL);
            exit(1);
        }

    //close(passwords);
    char p[20];
    while (fgets(p, 20, passwords)){
        char *nl = strchr(p, '\n');
        if(nl) *nl = '\0';
        
        
        char *hash = md5(p, strlen(p));
       
        if (strcmp (hash, targetHash) == 0){
            printf("Found %s == %s\n", hash, p);
            free(hash);
            break;
        }
        
    free(hash);
    
    }
   
   fclose(passwords);
    
    return NULL;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        fprintf(stderr, "Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

   
    FILE *in = fopen(argv[1], "r");
        if(!in){
            fprintf(stderr, "Can not open file %s for reading\n", argv[1]);
            perror(NULL);
            exit(0);
        }    
    char hash[40];
    while (fgets(hash, 40, in) != NULL){
        char *nl = strchr(hash, '\n');
        if(nl) *nl = '\0';
        
        crackHash(hash, argv[2]);
        
        
          
    } 
 
 fclose(in);
 
    
}
